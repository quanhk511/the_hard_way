// =============================
// Email: info@ebenmonney.com
// www.ebenmonney.com/templates
// =============================

export const environment = {
    production: true,
requireHttps: false ,
    baseUrl: 'https://10.10.0.5:5001' ,
    tokenUrl: 'https://10.10.0.5:5001/connect/token',
    loginUrl: '/login_prod'
}; 
