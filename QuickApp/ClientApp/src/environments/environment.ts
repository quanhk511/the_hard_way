// =============================
// Email: info@ebenmonney.com
// www.ebenmonney.com/templates
// =============================

// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
    production: false,
    requireHttps: false ,
    baseUrl: 'https://10.10.0.5:5001' , // Change this to the address of your backend API if different from frontend address
    tokenUrl: null , // For IdentityServer/Authorization Server API. You can set to null if same as baseUrl
    loginUrl: '/login_test' ,
};
