#!/bin/bash
SH=$(cd `dirname $BASH_SOURCE` && pwd)
AH=$(cd "$SH/../" && pwd)

source "$SH/.env"

    docker build   --file "$SH/Dockerfile"  -t $IMAGE_BE  $AH
