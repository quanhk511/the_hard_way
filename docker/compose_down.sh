#!/bin/bash
SH=$(cd `dirname $BASH_SOURCE` && pwd)
set -a
    source "$SH/.env"
    source "$SH/web_client/.env"

    docker-compose -f "$SH/docker-compose.yml" down

    docker ps --format 'Name: {{.Names}} | Ports: {{.Ports}} | Status: {{.Status}}'
