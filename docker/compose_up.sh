#!/bin/bash
SH=$(cd `dirname $BASH_SOURCE` && pwd)
set -a
    source "$SH/.env"
    source "$SH/web_client/.env"
    source "$SH/compose_down.sh"

    docker-compose -f "$SH/docker-compose.yml"  up  -d 

    docker ps --format 'Name: {{.Names}} | Ports: {{.Ports}} | Status: {{.Status}}'
