#!/bin/bash
SH=$(cd `dirname $BASH_SOURCE` && pwd)

docstring='''
docker context use remote_bizfly
docker info
docker context use default
'''
docker context create \
    --docker host=ssh://root@103.153.73.225 \
    --description="Remote bizfly" \
    remote_bizfly
