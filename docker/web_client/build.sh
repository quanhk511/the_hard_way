#!/bin/bash
docstring='
PARAM='-- --prod' ./build.sh
'

SH=$(cd `dirname $BASH_SOURCE` && pwd)
AH=$(cd "$SH/../.." && pwd)

source "$SH/.env"

    [ -z $PARAM ] && ( echo "PARAM is required"; kill $$ )

    docker build  \
                --build-arg BUILD_PARAM="$PARAM"  \
                --file "$SH/Dockerfile"  \
                -t $IMAGE_FE  $AH
