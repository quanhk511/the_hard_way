#!/usr/bin/env bash

env | grep -c "PORT"  # check env exist
    [ $? = 1 ] && (echo 'Envvar $PORT is required, please fill via prama -e into docker run'; kill $$)
        echo $PORT && envsubst '$PORT' < ./nginx.conf.tlp > ./nginx.conf && \
            cp ./nginx.conf /etc/nginx/nginx.conf && \
            cat /etc/nginx/nginx.conf  && \
            nginx -g 'daemon off;'